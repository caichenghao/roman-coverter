package test;
//@Author Chenghao Cai

import static org.junit.Assert.*;
import org.junit.*;
import org.junit.Test;

import roman.RomanConverter;

public class RomanConverterTest {
	private RomanConverter roman;
	@Test
	public void testToRoman() {
		//fail("Not yet implemented");
		roman = new RomanConverter();
		assertEquals("XXXII", roman.toRoman(32));
	}
	@Test(expected = IllegalArgumentException.class)
	public void testSmallOutOfRange() {
		roman = new RomanConverter();
		roman.toRoman(-100);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testLargeOutOfRange() {
		roman = new RomanConverter();
		roman.toRoman(9900);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalRoman() {
		roman = new RomanConverter();
		roman.fromRoman("BB");
	}
	
	@Test
	public void testFromRoman() {
		//fail("Not yet implemented");
		roman = new RomanConverter();
		assertEquals(123, roman.fromRoman("CXXIII"));
	}
}
